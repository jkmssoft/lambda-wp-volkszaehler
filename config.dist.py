config = {
    'heatPump' : {
        'channels' : {
            # channel: {'register': '', 'uuid': ''},

            'qpHeating' : {'register': 1011, 'uuid': ''},
            'setpointFlowLineTemp' : {'register': 5005, 'uuid': ''},
            'flowLineTemperature' : {'register': 1004, 'uuid': ''},
            'returnLineTemperature' : {'register': 1005, 'uuid': ''},
            'volSink' : {'register': 1006, 'uuid': '', 'scale': 0.001},
            #'volSink' : {'register': 1006, 'uuid': '', 'scale': 1/0.6}, # for old firmware l/min
            #'volSource' : {'register': 1009, 'uuid': ''},
            'compressorRating' : {'register': 1010, 'uuid': ''},
            'boilerActualHighTemp' : {'register': 2002, 'uuid': ''},
            'actualAmbientTemp' : {'register': 2, 'uuid': ''},
            'averageAmbientTemp1h' : {'register': 3, 'uuid': ''},
            'calculatedAmbientTemp' : {'register': 4, 'uuid': ''},
            'tEqIn' : {'register': 1007, 'uuid': ''},
            #'tEqOut' : {'register': 1008, 'uuid': ''},
            'fiPowerConsumption' : {'register': 1012, 'uuid': ''},
            'operatingState' : {'register': 1003, 'uuid': ''},
            'operatingStateString' : {'register': 1003, 'uuid': ''}, # only for json relevant
            'VdAEnergyElectricalkWh' : {'register': 1020, 'uuid': ''},
            'VdAEnergyHeatkWh' : {'register': 1022, 'uuid': ''},
            'COP' : {'register': 1013, 'uuid': ''}
        },
        'lambda': {
            'ip' : '192.168.178.220',
            'unitId': 0,# yes, 0 is correct
        },
        'summermodeChannels': ['boilerActualHighTemp', 'actualAmbientTemp'],
        'vzHost': 'http://toyourvlkszählerhost/htdocs/',
		'vzAdditionalParameter': '&abc=def', # empty variable if no param!
    }
}
conf = config['heatPump']
