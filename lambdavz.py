#!/usr/bin/env python3

"""Lambda data logger
Read data from lambda heat pump and send it to volkszähler or get as json.

usage: python3 lambdavz.py [-h] [--vz] [--summermode] [-n] [--json]

Read data from lambda heatpump and send it to your volkszähler. Alternative print data
as json.

optional arguments:
  -h, --help     show this help message and exit
  --vz           Send data to vz.
  --summermode   Send only limited registers data to volkszähler (but if
                 heatpump is running, all data will be pushed).
  -n, --dry-run  Do not send data to volkszähler, only dry Run only
  --json         Print the data as json.

"""

"""
TODO TO DO
- search for todo
- add -q quiet parameter and use for os.system('wget -O?
- get multiple registers with one modbus request
...
"""

from pyModbusTCP.client import ModbusClient
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
import time
import os
import sys
import json
import argparse
from config import *

class LambdaHeatPump:
    def getOperatingStateText(self, valueOperatingState):
        if (valueOperatingState == 0):
            return 'STBY'
        if (valueOperatingState == 1):
            return 'CH'    
        if (valueOperatingState == 2):
            return 'DHW'
        if (valueOperatingState == 3):
            return 'CC'
        if (valueOperatingState == 4):
            return 'CIRCULATE'
        if (valueOperatingState == 5):
            return 'DEFROST'
        if (valueOperatingState == 6):
            return 'OFF'
        if (valueOperatingState == 7):
            return 'FROST'
        if (valueOperatingState == 8):
            return 'STBY-FROST'
        if (valueOperatingState == 10):
            return 'SUMMER'
        if (valueOperatingState == 11):
            return 'HOLIDAY'
        if (valueOperatingState == 12):
            return 'ERROR'
        if (valueOperatingState == 13):
            return 'WARNING'
        if (valueOperatingState == 14):
            return 'INFO-MESSAGE'
        if (valueOperatingState == 15):
            return 'TIME-BLOCK'
        if (valueOperatingState == 16):
            return 'RELEASE-BLOCK'
        if (valueOperatingState == 17):
            return 'MINTEMP-BLOCK'

    def getData(self): # todo better return the data
        self.data = {}
       
        modbusConnection = ModbusClient(host=conf['lambda']['ip'], port=502, unit_id=conf['lambda']['unitId'], auto_open=True, auto_close=True)

        for channel, registeruuid in conf['channels'].items():
            register = registeruuid.get('register')
            uuid = registeruuid.get('uuid')
            try:
                scale = registeruuid.get('scale')
            except KeyError:
                scale = 1
           
            value = -1 # reset
            
            if args.vz == True and uuid == '': # only process if it has a uuid for volkszähler
                continue
            
            try:                   
                # Actual ambient temp.
                if channel == 'actualAmbientTemp':
                    regValue = modbusConnection.read_holding_registers(register, 1)
                    decoder = BinaryPayloadDecoder.fromRegisters(regValue, byteorder=Endian.Big, wordorder=Endian.Big)
                    value = decoder.decode_16bit_int() / 10.0

                # Average ambient temp. 1h
                if channel == 'averageAmbientTemp1h':
                    regValue = modbusConnection.read_holding_registers(register, 1)
                    decoder = BinaryPayloadDecoder.fromRegisters(regValue, byteorder=Endian.Big, wordorder=Endian.Big)
                    value = decoder.decode_16bit_int() / 10.0
                
                # Calculated ambient temp.
                if channel == 'calculatedAmbientTemp':
                    regValue = modbusConnection.read_holding_registers(register, 1)
                    decoder = BinaryPayloadDecoder.fromRegisters(regValue, byteorder=Endian.Big, wordorder=Endian.Big)
                    value = decoder.decode_16bit_int() / 10.0

                # Operating state
                if channel == 'operatingState':
                    regValue = modbusConnection.read_holding_registers(register, 1)
                    decoder = BinaryPayloadDecoder.fromRegisters(regValue, byteorder=Endian.Big, wordorder=Endian.Big)
                    value = decoder.decode_16bit_uint()

                # Operating state
                if channel == 'operatingStateString':
                    regValue = modbusConnection.read_holding_registers(register, 1)
                    decoder = BinaryPayloadDecoder.fromRegisters(regValue, byteorder=Endian.Big, wordorder=Endian.Big)
                    value = self.getOperatingStateText(decoder.decode_16bit_uint())

                # Flow line temperature T-flow
                if channel == 'flowLineTemperature':
                    regValue = modbusConnection.read_holding_registers(register, 1)
                    decoder = BinaryPayloadDecoder.fromRegisters(regValue, byteorder=Endian.Big, wordorder=Endian.Big)
                    value = decoder.decode_16bit_int() / 100.0 # 1250 => 12.5

                # Return line temperature T-return
                if channel == 'returnLineTemperature':
                    regValue = modbusConnection.read_holding_registers(register, 1)
                    decoder = BinaryPayloadDecoder.fromRegisters(regValue, byteorder=Endian.Big, wordorder=Endian.Big)
                    value = decoder.decode_16bit_int() / 100.0 # 1250 => 12.5

                # Vol. sink
                if channel == 'volSink':
                    regValue = modbusConnection.read_holding_registers(register, 1)
                    decoder = BinaryPayloadDecoder.fromRegisters(regValue, byteorder=Endian.Big, wordorder=Endian.Big)
                    value = decoder.decode_16bit_int() * scale

                # T-EQin
                if channel == 'tEqIn':
                    regValue = modbusConnection.read_holding_registers(register, 1)
                    decoder = BinaryPayloadDecoder.fromRegisters(regValue, byteorder=Endian.Big, wordorder=Endian.Big)
                    value = decoder.decode_16bit_int() / 100.0 

                # T-EQout
                if channel == 'tEqOut':
                    regValue = modbusConnection.read_holding_registers(register, 1)
                    decoder = BinaryPayloadDecoder.fromRegisters(regValue, byteorder=Endian.Big, wordorder=Endian.Big)
                    value = decoder.decode_16bit_int() / 100.0
                    if value < 100: # then invalid value
                        value = 0

                # Vol. source # maybe only needed for future heat pump versions
                if channel == 'volSource':
                    regValue = modbusConnection.read_holding_registers(register, 1)
                    decoder = BinaryPayloadDecoder.fromRegisters(regValue, byteorder=Endian.Big, wordorder=Endian.Big)
                    value = decoder.decode_16bit_int() / 10.0 * 60 / 1000 # 125 => 12.5 * 60 / 1000 => m³

                # Compressor unit rating Compressor-Rating 0.01%
                if channel == 'compressorRating':
                    regValue = modbusConnection.read_holding_registers(register, 1)
                    decoder = BinaryPayloadDecoder.fromRegisters(regValue, byteorder=Endian.Big, wordorder=Endian.Big)
                    value = decoder.decode_16bit_uint() / 100.0 # 100 => 0,01
                    
                # Actual heating capacity Qp heating
                if channel == 'qpHeating':
                    regValue = modbusConnection.read_holding_registers(register, 1)
                    decoder = BinaryPayloadDecoder.fromRegisters(regValue, byteorder=Endian.Big, wordorder=Endian.Big)
                    value = decoder.decode_16bit_int() / 10.0 # 12 => 1.2

                # FI power consumption 
                if channel == 'fiPowerConsumption':
                    regValue = modbusConnection.read_holding_registers(register, 1)
                    decoder = BinaryPayloadDecoder.fromRegisters(regValue, byteorder=Endian.Big, wordorder=Endian.Big)
                    value = decoder.decode_16bit_int() # watt

                # COP
                if channel == 'COP':
                    regValue = modbusConnection.read_holding_registers(register, 1)
                    decoder = BinaryPayloadDecoder.fromRegisters(regValue, byteorder=Endian.Big, wordorder=Endian.Big)
                    value = decoder.decode_16bit_int() / 100.0 # COP 0.01

                # Boiler Actual high temp.
                if channel == 'boilerActualHighTemp':
                    regValue = modbusConnection.read_holding_registers(register, 1)
                    decoder = BinaryPayloadDecoder.fromRegisters(regValue, byteorder=Endian.Big, wordorder=Endian.Big)
                    value = decoder.decode_16bit_int() / 10.0

                # Statistic VdA E since last reset
                if channel == 'VdAEnergyElectricalkWh':
                    regValue = modbusConnection.read_holding_registers(register, 2)
                    decoder = BinaryPayloadDecoder.fromRegisters(regValue, byteorder=Endian.Big, wordorder=Endian.Big)
                    value = decoder.decode_32bit_int() / 1000.0 # Wh to kWh
                
                # Statistic VdA Q since last reset
                if channel == 'VdAEnergyHeatkWh':
                    regValue = modbusConnection.read_holding_registers(register, 2)
                    decoder = BinaryPayloadDecoder.fromRegisters(regValue, byteorder=Endian.Big, wordorder=Endian.Big)
                    value = decoder.decode_32bit_int() / 1000.0 # Wh to kWh

                # Setpoint flow line temp.
                if channel == 'setpointFlowLineTemp':
                    regValue = modbusConnection.read_holding_registers(register, 1)
                    decoder = BinaryPayloadDecoder.fromRegisters(regValue, byteorder=Endian.Big, wordorder=Endian.Big)
                    value = decoder.decode_16bit_int() / 10.0 # 125 => 12.5
                    if int(value) == -300: # if the value is -300, then set it to 0
                        value = 0
                
                #test print(f"\"{channel}\" value: {value}")
                
                # TODO if required add more registers here and in config.dist.py

            except Exception as e:
                print('get data failed, modbus error: %s' % (e))

            # collect the data
            self.data[channel] = value  

    def submitVz(self):
        # submit the collected data to volkszähler
        # detect if heatpump is heating and if so, disable logOnlySummerMode (then log everything)
        self.getData()

        logOnlySummerMode = True
        if self.data['compressorRating'] > 0:
            logOnlySummerMode = False

        if args.summermode and logOnlySummerMode:
            print('summermode detected')
        
        unixtimestampms = (int) (time.time()) * 1000 # second => milliseconds
        
        # submit data to VZ
        for channel, registeruuid in conf['channels'].items():
            try:
                value = self.data[channel]
            except KeyError:
                continue
            
            # is it a channel logged in summer mode?
            summerChannel = True
            if args.summermode and logOnlySummerMode and not channel in conf['summermodeChannels']:
                continue
                
            print(f"\"{channel}\" value: {value}")
            vzurl = f"{conf['vzHost']}middleware.php/data/{registeruuid['uuid']}.json?operation=add&ts={unixtimestampms}&value=%f{conf['vzAdditionalParameter']}" % (value)

            # dry run? then do not submit, show only url
            if args.dryrun:
                print('dry run!')
                print('dry: %s' % (vzurl))
            else: # send data to vz
                os.system('wget -O /dev/null \''+vzurl+'\'')
            #for
        #endif

    def getJson(self):
        # print the collected data as json
        self.getData()
        print(json.dumps(self.data))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Get data from lambda heatpump and send it to your volkszähler. Alternative print data as json.')
    parser.add_argument('--vz', dest='vz', action='store_true', help='Send data to vz.')
    parser.add_argument('--summermode', dest='summermode', action='store_true', help='Send only limited registers data to volkszähler (but if heatpump is running, all data will be pushed).')
    parser.add_argument('-n', '--dry-run', dest='dryrun', action='store_true', help='Do not send data to volkszähler, only dry Run only', default=False)
    parser.add_argument('--json', dest='json', action='store_true', help='Print the data as json.')

    # detect if no param, then print help
    if not len(sys.argv) > 1:
        parser.print_help()
        exit(1)

    args = parser.parse_args()
    
    lambdaHeatPump = LambdaHeatPump()
    
    if args.json == True:
        lambdaHeatPump.getJson()
    if args.vz == True:
        lambdaHeatPump.submitVz()
