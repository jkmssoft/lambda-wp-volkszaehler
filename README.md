# Lambda-wp Volkszähler (Lambda Wärmepumpe EUL)

Script to get data from [lambda heatpump (EU08L, EU13L, EU15L, ...)](https://lambda-wp.at/) and send it to your [volkszähler](https://github.com/volkszaehler/volkszaehler.org).   
For longterm data logging, the integrated logger in the control unit logs only 24 hours.

# Installation
pip3 install -r requirements.txt

# Usage
Copy `config.dist.py` to `config.py` and set your configuration.

Parameter:
```
lambdavz.py
  -h, --help     show this help message and exit
  --vz           Send data to vz.
  --summermode   Send only limited registers data to volkszähler (but if
                 heatpump is running, all data will be pushed).
  -n, --dry-run  Do not send data to volkszähler, only dry Run only
  --json         Print the data as json.
```

# More Info
Modbus register list:
https://lambda-wp.at/wp-content/uploads/2023/04/Modbus-Protokoll-und-Beschreibung.pdf

Old modbus register list:
https://lambda-wp.at/wp-content/uploads/2022/01/Modbus-Protokoll-und-Beschreibung.pdf

# Screenshot
What it could look like:
![heatpump-volkszaehler](.gitlab/images/Screenshot 2023-09-14 at 16-47-55 volkszaehler.org - web frontend.png)
